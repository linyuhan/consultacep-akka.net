﻿using Microsoft.AspNetCore.Mvc;

namespace WebCep.Application.Controllers
{
    public class HomeController : Controller
    {
        public static readonly string Version = typeof(HomeController).Assembly.GetName().Version.ToString();

        public IActionResult Index()
        {
            ViewBag.AppVersion = Version;
            return View();
        }
    }
}

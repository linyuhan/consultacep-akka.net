﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebCep.Application.Dtos;
using WebCep.Core;
using Akka.Actor;
using System;
using WebCep.Application.Actors;
using System.Threading.Tasks;

namespace WebCep.Application.Controllers
{
    [Route("api/pigeonry")]
    [ApiController]
    public class PigeonryController : Controller
    {
        
        private readonly IMapper _mapper;

        public PigeonryController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpGet("buscar")]
        public async Task<ActionResult<OutEndereco>> GetEnderecoAsync(string cep)
        {
            var endereco = await SystemActors.Pigeon.Ask<Endereco>(cep, TimeSpan.FromSeconds(2));
            return _mapper.Map<OutEndereco>(endereco);
        }
    }

}

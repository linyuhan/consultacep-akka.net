﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCep.Application.Dtos;
using WebCep.Core;

namespace WebCep.Application
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Endereco, OutEndereco>();
        }
    }
}

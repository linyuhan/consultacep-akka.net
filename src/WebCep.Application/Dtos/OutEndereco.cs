﻿using WebCep.Core;

namespace WebCep.Application.Dtos
{
    public class OutEndereco
    {
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public UFEnum UF { get; set; }
    }
}

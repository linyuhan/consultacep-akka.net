using Akka.Actor;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebCep.Application;
using WebCep.Application.Actors;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace WebCep
{
    public class Startup
    {
        public static ActorSystem ActorSystem;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });



            ActorSystemRefs.ActorSystem = ActorSystem.Create("webcep");
            var actorSystem = ActorSystemRefs.ActorSystem;

            IActorRef bloodhoundActor = actorSystem.ActorOf(Props.Create(() => new Bloodhound.Service.Bloodhound()),
                "bloodhound");
            SystemActors.Pigeon = actorSystem.ActorOf(Props.Create(() =>
            new Pigeon.Service.Pigeon(bloodhoundActor)), "pigeon");

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default",
                    "{controller=Home}/{action=Index}/{id?}");
            });
            
        }
    }
}

﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;
using WebCep.Core;
using WebCep.Services.Entities;

namespace WebCep.Services
{
    public class ViaCepClient
    {
        private static string Uri = "http://viacep.com.br/ws/";
        public ViaCepClient()
        {
        }

        public Endereco AddressSearch(string cep)
        {
            var client = new RestClient($"{Uri}{cep}/json/") { Timeout = 3000 };
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);

            if (response.Content.Contains("cep"))
            {
                var addr = JsonConvert.DeserializeObject<ViaCepResponse>(response.Content);
                return new Endereco()
                {
                    Bairro = addr.Bairro,
                    Cep = addr.Cep,
                    Cidade = addr.Localidade,
                    UF = Enum.Parse<UFEnum>(addr.Uf),
                    Complemento = addr.Complemento,
                    Logradouro = addr.Logradouro,
                    Numero = ""
                };
            }
            else
            {
                return null;
            }
        }

    }
}

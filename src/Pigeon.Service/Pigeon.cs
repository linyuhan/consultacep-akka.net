﻿using Akka.Actor;

namespace Pigeon.Service
{
    public class Pigeon : ReceiveActor, IWithUnboundedStash
    {
        private readonly IActorRef _bloodhound;
        public IStash Stash { get; set; }

        public Pigeon(IActorRef seeker)
        {
            _bloodhound = seeker;
            StartWork();
        }

        private void StartWork()
        {
            Receive<string>(str => { //escuta tudo que for string
                _bloodhound.Forward(str); //manda pro bloodhound
            });
        }
    }

}

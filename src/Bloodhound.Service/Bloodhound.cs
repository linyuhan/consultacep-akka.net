﻿
using Akka.Actor;
using System;
using WebCep.Core;
using WebCep.Services;

namespace Bloodhound.Service
{
    public class Bloodhound : ReceiveActor
    {
        private ViaCepClient _viaCepClient;

        public Bloodhound()
        {
            Receives();
        }

        private void Receives()
        {
            //escuta tudo que for string
            Receive<string>(cep =>
            {
                _viaCepClient = new ViaCepClient();//bloodhound procura
                Endereco endereco = _viaCepClient.AddressSearch(cep);
                Sender.Forward(endereco);//quando encontra, envia pra quem mandou a string
            });
        }

    }
}
